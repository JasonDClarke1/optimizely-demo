import React from 'react';
import {
    withOptimizely
} from '@optimizely/react-sdk';

const A = (props) => (
    <button onClick={()=> props.optimizely.track('purchaseA')}>
        A
    </button>
)

const wrappedA = withOptimizely(A);

export default wrappedA;