import React from 'react'; 
import {
    OptimizelyExperiment,
    OptimizelyVariation,
} from '@optimizely/react-sdk';
import A from './A';
import B from './B';
import Default from './Default';

//https://app.optimizely.com/v2/projects/17731381317/experiments/17752130631/variations
const ABTest = (props) => {
    return (
      <OptimizelyExperiment experiment="JasonReactTest">
        <OptimizelyVariation variation="variation_1">
          <A />
        </OptimizelyVariation>
        <OptimizelyVariation variation="variation_2">
          <B />
        </OptimizelyVariation>
        <OptimizelyVariation default>
          {Default? <Default />: <A />}
        </OptimizelyVariation>
      </OptimizelyExperiment>
    )
  }

export default ABTest;