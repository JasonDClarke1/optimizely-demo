import React from 'react';
import {
    withOptimizely
} from '@optimizely/react-sdk';

const B = (props) => (
    <button onClick={()=> props.optimizely.track('purchaseB')}>
        B
    </button>
)

const wrappedB = withOptimizely(B);

export default wrappedB;