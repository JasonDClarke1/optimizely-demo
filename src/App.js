import React from 'react';

//setup
import {
  createInstance,
  OptimizelyProvider,
} from '@optimizely/react-sdk';
import optimizelyInstanceConfig from './optimizelyInstanceConfig';

// lots of users
import users from './users';

//examples
import ABTest from './ABTest/ABTest';
import Feature from './Feature/Feature';

//outputting metadata onto screen
import LogUser from './Logging/LogUser';
import LogConfig from './Logging/LogConfig';


function App() {
  const experiences = users.map((user) => {
    // Note: Normally only one instance of Optimizely is created with createInstance in an application
    // at the start of the application. However, this application simulates multiple experiences for educational
    // purposes
    const optimizely = createInstance(optimizelyInstanceConfig);

    // Note: Normally OptimizelyProvider is only used once to wrap an entire application
    // But this app simulates multiple experiences for educational purposes
    return (
      <>
      <LogUser user={user} />
      <OptimizelyProvider
        optimizely={optimizely}
        user={user}
        key={user.id}
      >
        <Feature />
        <ABTest />
      </OptimizelyProvider>
      </>
    )
  });

  return (
    <>
      <LogConfig />
      { experiences }
    </>
  );
}

export default App;