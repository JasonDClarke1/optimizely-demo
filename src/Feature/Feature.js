import React from 'react';
import {OptimizelyFeature} from '@optimizely/react-sdk';

//https://app.optimizely.com/v2/projects/17731381317/experiments/17724191509/variations
const Feature = ()=> (
    <OptimizelyFeature feature="JasonReactFeature">
    {(isEnabled, variables) => {
        const {bool,price,age,colour} = variables;
        // this is the actual feature
        if (isEnabled) {
        return (
            <div>
                {`${bool}`}, {price}, {age}, {colour}
            </div>
        )
        }
        return null
    }}
    </OptimizelyFeature>
)

export default Feature;