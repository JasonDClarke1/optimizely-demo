import React from 'react';
import optimizelyInstanceConfig from '../optimizelyInstanceConfig';

const LogConfig = () => (
  <div> 
    sdkKey: {optimizelyInstanceConfig.sdkKey}
    {Object.entries(optimizelyInstanceConfig.datafileOptions).map(entry=> <span>{entry[0]}: {entry[1]}</span>)}
  </div>
)

export default LogConfig;