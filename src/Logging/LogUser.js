import React from 'react';

//displays user attributes for educational purposes
const LogUser = ({user}) => (
  <div>id: {user.id}. country: {user.attributes.country} device: {user.attributes.device}</div>
)

export default LogUser;