Documentation:
https://docs.developers.optimizely.com/full-stack/docs/

In particular for react:
https://docs.developers.optimizely.com/full-stack/docs/javascript-react-sdk
https://github.com/optimizely/react-sdk

This project started from 'getting started'
https://app.optimizely.com/v2/projects/17731381317/onboarding?step=0&language=node


Then set up some stuff from the optimizeley console, setting up a feature and an A/B test
https://app.optimizely.com/v2/projects/17731381317/experiments
(As of now)
JasonReactFeature - feature
JasonReactTest - test
It depends on the configuration of the experiments.
They need to be configured, also running in development environment
Also need to run yarn start locally

You can also set up and see insights (click conversion rate, etc)
For example
https://app.optimizely.com/v2/projects/17731381317/results/17741212709?previousView=EXPERIMENTS

See also quickstart
https://docs.developers.optimizely.com/full-stack/docs/javascript-react

See core concepts
https://docs.developers.optimizely.com/full-stack/docs/review-core-concepts