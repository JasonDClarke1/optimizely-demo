// here 10 visitors are hardcoded so we can see
// potentially 10 different versions of the app.
// in a real app there will only be one visitor at a time. 
// see https://app.optimizely.com/v2/projects/17731381317/audiences/attributes
// set audiences with attributes
// https://app.optimizely.com/v2/projects/17731381317/experiments/17752130631/audiences
// device is mobile, tablet, desktop
// country is US or UK

const users = [
    { id: 'alice',
      attributes: {
        country: 'UK',
        device: 'tablet',
      }
  },
    { id: 'bob', 
      attributes: {
        country: 'US',
        device: 'mobile',
      }
    },
    { id: 'charlie',
      attributes: {
        country: 'UK',
        device: 'desktop',
      }
    },
    { id: 'don',
      attributes: {
        country: 'US',
        device: 'desktop',
      }
    },
    { id: 'eli',
      attributes: {
        country: 'US',
        device: 'mobile',
      }
    },
    { id: 'fabio', 
      attributes: {
        country: 'US',
        device: 'tablet',
      }
    },
    { id: 'gary',    
      attributes: {
        country: 'UK',
        device: 'desktop',
      }
    },
    { id: 'helen',
      attributes: {
        country: 'UK',
        device: 'tablet',
      }
    },
    { id: 'ian',     
      attributes: {
        country: 'UK',
        device: 'mobile',
      }
    },
    { id: 'jill',    
      attributes: {
        country: 'US',
        device: 'mobile',
      }
    },
  ];
  //see whitelist
  //https://app.optimizely.com/v2/projects/17731381317/experiments/17752130631/whitelist

export default users;